import datetime

import numpy as np

# GLOBAL VAR #
database = "data/database.csv"


class ReadData:
    def __init__(self, date):
        self.date = date.replace('/', '-')
        self.mean = []
        self.max = []
        for i in range(24):
            self.mean.append([])
            self.max.append([])

    def update_data(self):
        for i in range(len(self.mean)):
            if len(self.mean[i]) != 0:
                self.max[i] = np.round(np.max(self.mean[i]) * 3.6, 1)
                self.mean[i] = np.round(np.mean(self.mean[i]) * 3.6, 1)
            else:
                self.max[i] = ""
                self.mean[i] = ""


class LoadData:
    def __init__(self, data):
        self.date = data[0]
        self.mean = []
        self.max = []
        for i in range(24):
            self.mean.append(data[2 * i + 1])
            self.max.append(data[2 * (i + 1)])


def save_data(file):
    """
    Data is stored with the format : yyyy-mm-dd, mean1, max1, mean2, max2, etc...
    :param file:
    :return:
    """
    file_np = np.loadtxt(file, dtype=str, delimiter="\t", skiprows=1, usecols=(0, 1, 2, 3))
    days = []
    actual_day = file_np[0][1]
    days.append(ReadData(actual_day))
    number_of_days = 0
    for measure in file_np:
        if actual_day == measure[1]:
            days[number_of_days].mean[int(measure[2][:2])].append(
                float(measure[3].replace(' ', '').replace(',', '.')))
        else:
            actual_day = measure[1]
            number_of_days += 1
            days.append(ReadData(actual_day))
    for day in days:
        day.update_data()
        loadday = search_database(day.date)
        if loadday[1]:
            merge_data(loadday[0], day)
            delete_line(day)
        with open(database, 'a') as f:
            f.write(day.date + ',')
            for i in range(23):
                f.write(str(day.mean[i]) + ',' + str(day.max[i]) + ',')
            f.write(str(day.mean[23]) + ',' + str(day.max[23]) + '\n')


def merge_data(day_database, day_add):
    for i in range(24):
        if day_add.mean[i] == "":
            day_add.mean[i] = day_database.mean[i]
            day_add.max[i] = day_database.max[i]
        elif day_database.mean[i] != "" and day_add.mean[i] != "":
            day_add.mean[i] = np.round((float(day_database.mean[i]) + float(day_add.mean[i]))/2, 1)
            day_add.max[i] = np.round(max(float(day_database.max[i]), float(day_add.max[i])), 1)


def delete_line(day: ReadData):
    with open(database, 'r') as f:
        lines = f.readlines()
    with open(database, 'w') as f:
        for line in lines:
            if line[:10] != day.date:
                f.write(line)


def search_database(date):
    """
    Return LoadData of day and true if found, 0 and false either way
    :param date:
    :return:
    """
    try:
        open(database, 'r')
    except:
        open(database, 'w')
    database_np = np.genfromtxt(database, dtype=str, delimiter=',')
    for i in range(len(database_np)):
        if database_np[i][0] == date:
            return LoadData(database_np[i]), True
    return 0, False


def add_day(date):
    day_date = datetime.date.fromisoformat(date)
    timedelta = datetime.timedelta(days=1)
    new_date = day_date + timedelta
    return new_date.isoformat()

import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import tkcalendar
import datetime
import os
import xlsxwriter

import data_handling as dh


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.grid()
        self.week_table = WeekTable(self)
        self.week_table.grid(row=0, column=0, rowspan=3)
        self.calendar = DatePicker(self)
        self.calendar.grid(row=0, column=1, columnspan=3)
        tk.Button(self, text="Load", width=15, height=10, command=self.load_button).grid(row=2, column=1)
        tk.Button(self, text="Print", width=15, height=10, command=self.print_file).grid(row=2, column=2)
        self.load_file_button = LoadFile(self)
        self.load_file_button.grid(row=1, column=1, columnspan=2)
        self.load_button()
        tk.Label(text="Affichage des vitesses en km/h").grid(row=3, column=0)

    def load_button(self):
        self.calendar.get_week()
        self.week_table.week_display.config(
            text="SEMAINE DU " + invert_date_format(self.calendar.monday) + " AU " + invert_date_format(
                self.calendar.sunday))
        date_to_search = self.calendar.monday
        for i in range(7):
            loadday = dh.search_database(date_to_search)
            if loadday[1]:
                self.week_table.days_table[i].set_day(loadday[0])
            else:
                self.week_table.days_table[i].set_na_day()
            date_to_search = dh.add_day(date_to_search)

    def print_file(self):
        self.calendar.get_week()
        date_to_search = self.calendar.monday

        workbook = xlsxwriter.Workbook('data/print.xlsx')
        worksheet = workbook.add_worksheet()
        worksheet.set_column(1, 15, 11)
        worksheet.set_landscape()
        worksheet.set_margins(0, 0, 2, 2)
        worksheet.print_area("A1:O28")
        worksheet.fit_to_pages(1, 1)

        hour = workbook.add_format({'bold': True, 'align': 'center', 'fg_color': '#DCE6F1'})
        day = workbook.add_format({'bold': True, 'align': 'center', 'fg_color': '#C5D9F1'})
        vent = workbook.add_format({'align': 'center', 'fg_color': '#C5D9F1'})
        title = workbook.add_format({'bold': True, 'align': 'center', 'fg_color': '#FDE9D9', 'font_size': 20})
        hours = workbook.add_format({'align': 'center', 'fg_color': '#DCE6F1'})
        center=workbook.add_format({'align': 'center'})

        worksheet.write(2, 0, "Heures", hour)
        worksheet.merge_range('B1:O1', "SEMAINE DU " + invert_date_format(self.calendar.monday) + " AU " + invert_date_format(
                self.calendar.sunday), title)
        worksheet.merge_range('B2:C2', "Lundi", day)
        worksheet.merge_range('D2:E2', "Mardi", day)
        worksheet.merge_range('F2:G2', "Mercredi", day)
        worksheet.merge_range('H2:I2', "Jeudi", day)
        worksheet.merge_range('J2:K2', "Vendredi", day)
        worksheet.merge_range('L2:M2', "Samedi", day)
        worksheet.merge_range('N2:O2', "Dimanche", day)
        for i in range(24):
            worksheet.write(3+i, 0, str(i) + '-' + str(i+1), hours)
        for i in range(7):
            worksheet.write(2, 1+2*i, 'Vent moyen', vent)
            worksheet.write(2, 1+(2*i+1), 'Vent max', vent)
            loadday = dh.search_database(date_to_search)
            if loadday[1]:
                for j in range(24):
                    worksheet.write(3+j, 1+2*i, loadday[0].mean[j], center)
                    worksheet.write(3+j, 1+(2*i+1), loadday[0].max[j], center)
            date_to_search = dh.add_day(date_to_search)
        worksheet.merge_range('N28:O28', "Vitesses en km/h", hours)
        workbook.close()
        os.system('start excel.exe "data/print.xlsx"')


class LoadFile(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent, padx=60)
        tk.Button(self, text="Add a file", width=40, command=self.file_opener).grid()

    @staticmethod
    def file_opener():
        with filedialog.askopenfile(title="Select file to add to the database", mode='r', filetypes=[('TSV files',
                                                                                                      '*.XLS')]) as f:
            dh.save_data(f)


class DayTable(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent, padx=2)
        self.iid = []

        self.cols = ("Vent moyen", "Vent max")
        style=ttk.Style()
        style.configure("mystyle.Treeview", font=('Times', 11, 'bold'))
        self.day_tree = ttk.Treeview(self, style="mystyle.Treeview", columns=self.cols, show="headings", height=24)
        for col in self.cols:
            self.day_tree.heading(col, text=col)
            self.day_tree.column(col, width=80)
        self.init_day()
        self.day_tree.grid()

    def init_day(self):
        for i in range(24):
            self.iid.append(self.day_tree.insert('', index=i, values=('', '')))

    def set_day(self, day):
        for i in range(24):
            self.day_tree.item(self.iid[i], values=(day.mean[i], day.max[i]))

    def set_na_day(self):
        for i in range(24):
            self.day_tree.item(self.iid[i], values=("", ''))


class WeekTable(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent, padx=19, pady=10)
        days = ("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche")
        self.days_table = []
        self.week_display = ttk.Label(self, text="", width=16 * 7, font=("-*-lucidabright-demibold-r-*-*-17-*-*-*-*-*-*-*"), anchor=tk.CENTER,
                                      padding=15)
        self.week_display.grid(row=0, column=0, columnspan=8)
        ttk.Label(self, text="Heures", font=("Times", 11, "bold"), width=15, relief="groove", anchor=tk.CENTER).grid(
            row=2, column=0)
        for i in range(len(days)):
            ttk.Label(self, text=days[i], width=18, anchor=tk.CENTER, relief="groove", font=("-*-lucidabright-medium-r-*-*-*-140-*-*-*-*-*-*", 10),
                      padding=1).grid(row=1, column=i + 1)
            self.days_table.append(DayTable(self))
            self.days_table[i].grid(row=2, column=i + 1, rowspan=25)
        for i in range(24):
            ttk.Label(self, text=str(i) + "-" + str((i + 1) % 24), width=10, anchor=tk.CENTER, relief="ridge", font=("-*-lucidabright-medium-r-*-*-*-140-*-*-*-*-*-*", 10)).grid(
                row=i + 3, column=0)


class DatePicker(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent, padx=10, pady=70)
        self.cal = tkcalendar.Calendar(self, selectmode='day', date_pattern='dd-MM-yyyy')
        self.cal.grid()
        self.display_date = tk.Label(self, text="", pady=5, font='Times')
        self.display_date.grid()
        self.monday = ''
        self.sunday = ''

    def get_week(self):
        selected_date = self.get_date()
        iso_date = selected_date[-4:] + '-' + selected_date[3:5] + '-' + selected_date[:2]
        day_date = datetime.date.fromisoformat(iso_date)
        weekday = day_date.weekday()
        timedelta_monday = datetime.timedelta(days=weekday)
        timedelta_sunday = datetime.timedelta(days=6 - weekday)
        monday = day_date - timedelta_monday
        sunday = day_date + timedelta_sunday
        self.monday = monday.isoformat()
        self.sunday = sunday.isoformat()

    def get_date(self):
        return self.cal.get_date()


def invert_date_format(date):
    return date[-2:] + '-' + date[5:7] + '-' + date[:4]


root = tk.Tk()
app = Application(master=root)
app.master.title("Wind Measurements")
app.master.minsize(1000, 600)
app.mainloop()
